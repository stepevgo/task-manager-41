package ru.t1.stepanishchev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (row_id, created, name, descrptn, status, user_id) " +
            "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId});")
    void add(@Nullable final Project model);

    @Insert("INSERT INTO tm_project (row_id, created, name, descrptn, status, user_id) " +
            "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId});")
    void addByUser(
            @Param("userId") @Nullable String userId,
            @Nullable final Project model
    );

    @Nullable
    @Select("SELECT * FROM tm_project;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "description", column = "descrptn")
    })
    List<Project> findAll();

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId};")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    List<Project> findAllByUser(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY name;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    List<Project> findAllOrderByName(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    List<Project> findAllOrderByCreated(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY status;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    List<Project> findAllOrderByStatus(@Param("userId") @Nullable String userId);

    @NotNull
    @Select("SELECT COUNT(1) = 1 FROM tm_project WHERE row_id = #{id} AND user_id = #{userId};")
    Boolean existsByIdUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT * from tm_project WHERE row_id = #{id} AND user_id = #{userId}  LIMIT 1;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    Project findOneById(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Delete("DELETE FROM tm_project WHERE row_id = #{id} and user_id = #{userId};")
    void removeOneById(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Update("UPDATE tm_project SET created = #{created}, name = #{name}, descrptn = #{description}, status = #{status}, user_id = #{userId} WHERE row_id = #{id};")
    void update(@Param("userId") @Nullable String userId, @NotNull Project model);

    @Delete("TRUNCATE TABLE tm_project;")
    void removeAll();

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void removeAllByUserId(@NotNull @Param("userId") String userId);

    @Select("SELECT COUNT(1) FROM tm_project WHERE user_id = #{userId};")
    long getSize(@Param("userId") @Nullable String userId);

}