package ru.t1.stepanishchev.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class DataJsonLoadJaxBResponse extends AbstractResponse {
}