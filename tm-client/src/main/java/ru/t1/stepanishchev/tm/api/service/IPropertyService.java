package ru.t1.stepanishchev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull String getHost();

    @NotNull String getPort();
}